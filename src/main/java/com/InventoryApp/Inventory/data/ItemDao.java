package com.InventoryApp.Inventory.data;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.InventoryApp.Inventory.logic.Item;

public interface ItemDao extends PagingAndSortingRepository<Item, Long>{
	
}
