package com.InventoryApp.Inventory.jpa;

import java.util.List;

import com.InventoryApp.Inventory.logic.Item;

public interface ItemService {

	Item addNewItem(Item item) throws Exception;

	List<Item> getAllItemsWithPagination(int page, int size) throws Exception;

	Item getItemById(Long id) throws Exception;

	void deleteItem(Long id) throws Exception;

	Item updateItem(Long itemId, Item item) throws Exception;

	long getSize();

}
