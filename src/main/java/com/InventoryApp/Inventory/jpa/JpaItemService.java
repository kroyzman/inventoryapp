package com.InventoryApp.Inventory.jpa;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.InventoryApp.Inventory.data.ItemDao;
import com.InventoryApp.Inventory.exceptions.ItemNotFoundException;
import com.InventoryApp.Inventory.exceptions.ItemQuantityBelowZeroException;
import com.InventoryApp.Inventory.logic.Item;

@Service
public class JpaItemService implements ItemService {

	private ItemDao items;

	@Autowired
	public JpaItemService(ItemDao items) {
		this.items = items;
	}

	@Override
	@Transactional
	public Item addNewItem(Item item) throws Exception {
		return this.items.save(item);
	}

	@Override
	@Transactional
	public List<Item> getAllItemsWithPagination(int page, int size) throws Exception {
		Pageable reqPage = PageRequest.of(page, size);
		return (List<Item>) this.items.findAll(reqPage).toList();
	}

	@Override
	@Transactional
	public Item getItemById(Long id) throws Exception {
		return this.items.findById(id)
				.orElseThrow(() -> new ItemNotFoundException("Item doesn't exist with id: " + id));
	}

	@Override
	@Transactional
	public void deleteItem(Long id) throws Exception {
		this.items.deleteById(id);
	}

	@Override
	@Transactional
	public Item updateItem(Long itemId, Item itemNewValues) throws Exception {
		Item existingItem = this.getItemById(itemId);
		
		if(existingItem == null) {
			throw new ItemNotFoundException("Item doesn't exist with id: " + itemId);
		}
		
		if (itemNewValues.getAmount() != existingItem.getAmount()) {
			if(itemNewValues.getAmount() < 0) {
				throw new ItemQuantityBelowZeroException("Item quantity cant be negative!");
			}
			existingItem.setAmount(itemNewValues.getAmount());
		}

		return this.items.save(existingItem);
	}

	@Override
	@Transactional
	public long getSize() {
		return this.items.count();
	}

}
