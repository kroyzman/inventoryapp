package com.InventoryApp.Inventory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.InventoryApp.Inventory.data.ItemDao;
import com.InventoryApp.Inventory.logic.Item;



@SpringBootApplication

public class InventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner demo(ItemDao itemDao) {
		return (args) -> {
			Item item1 = new Item("Item1", 12, "i123");
			Item item2 = new Item("Item2", 2, "i123");
			Item item3 = new Item("Item3", 18, "i123");
			Item item4 = new Item("Item4", 32, "i123");
			Item item5 = new Item("Item5", 41, "i123");
			Item item6 = new Item("Item6", 114, "i123");
			Item item7 = new Item("Item7", 33, "i123");
			Item item8 = new Item("Item8", 4, "i123");
			Item item9 = new Item("Item9", 15, "i123");
			Item item10 = new Item("Item10", 1, "i123");
			Item item11 = new Item("Item11", 9, "i123");
			Item item12 = new Item("Item12", 100, "i123");
			
			itemDao.save(item1);
			itemDao.save(item2);
			itemDao.save(item3);
			itemDao.save(item4);
			itemDao.save(item5);
			itemDao.save(item6);
			itemDao.save(item7);
			itemDao.save(item8);
			itemDao.save(item9);
			itemDao.save(item10);
			itemDao.save(item11);
			itemDao.save(item12);
		};
	}

}
