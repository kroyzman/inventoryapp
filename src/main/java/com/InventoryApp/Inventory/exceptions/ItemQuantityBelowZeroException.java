package com.InventoryApp.Inventory.exceptions;

public class ItemQuantityBelowZeroException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1175176214122735410L;

	public ItemQuantityBelowZeroException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemQuantityBelowZeroException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ItemQuantityBelowZeroException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ItemQuantityBelowZeroException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ItemQuantityBelowZeroException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
