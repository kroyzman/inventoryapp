package com.InventoryApp.Inventory.logic;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item {
	private Long id;
	private String name;
	private long amount;
	private String inventoryCode;
	
	public Item() {
	}

	public Item(String name, long amount, String inventoryCode) {
		super();
		this.name = name;
		this.amount = amount;
		this.inventoryCode = inventoryCode;
	}
	
	
	
	public Item(long amount) {
		this.amount = amount;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getInventoryCode() {
		return inventoryCode;
	}

	public void setInventoryCode(String inventoryCode) {
		this.inventoryCode = inventoryCode;
	}
	
	
	
	
	
}
