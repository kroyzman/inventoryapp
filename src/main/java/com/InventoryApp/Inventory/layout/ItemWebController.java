package com.InventoryApp.Inventory.layout;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.InventoryApp.Inventory.jpa.ItemService;
import com.InventoryApp.Inventory.logic.Item;

@RestController
@CrossOrigin
public class ItemWebController {

	private ItemService itemService;

	@Autowired
	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/inventory/add", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Item addNewItem(@RequestBody Item item) throws Exception {
		System.out.println("adding item...");
		return this.itemService.addNewItem(item);
	}
	
	//default pagination size = 5
	@RequestMapping(method = RequestMethod.GET, path = "/inventory", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Item> getAllItems(@RequestParam(name = "size", required = false, defaultValue = "5") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) throws Exception {
		System.out.println("Getting Items...");
		List<Item> itemsList = this.itemService.getAllItemsWithPagination(page, size);
		return itemsList;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/inventory/item/{itemId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Item getItemById(@PathVariable("itemId") Long id) throws Exception {

		Item reqItem = this.itemService.getItemById(id);
		return reqItem;
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/inventory/delete/{itemId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteItem(@PathVariable("itemId") Long id) throws Exception {
		System.out.println("Deleting Item...");
		this.itemService.deleteItem(id);
	}

	// Update api for withdrawal/deposit amount.
	@RequestMapping(method = RequestMethod.PUT, path = "/inventory/update/{itemId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Item updateDealer(@PathVariable("itemId") Long itemId, @RequestBody Item item) throws Exception {
		System.out.println("Updating Items...");
		return this.itemService.updateItem(itemId, item);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/inventory/items/size", produces = MediaType.APPLICATION_JSON_VALUE)
	public long getItemsSize() throws Exception {
		System.out.println("Getting Items size...");
		return this.itemService.getSize();
	}

}
